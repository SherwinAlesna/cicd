import React from 'react'
import ReactDOM from 'react-dom'
import Table from '../ironman/Table'
import Bridge from '../ironman/Bridge'
import HtmlTableToJson from 'html-table-to-json'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import { shallow } from 'enzyme'
import expect from 'expect'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import Drawer from 'material-ui/Drawer'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'

Enzyme.configure({adapter: new Adapter()})

describe('<Table/> Component', () => {
  const state = {
    header: ["project_id", "id", "name", "standard_attr_id"],
    table: [{
      project_id: "qeowqio", id: '0320ewq', name: 'aoi231', standard_attr_id: 'qiewoq' 
    }],
    head: ["project_id", "id", "name", "standard_attr_id"]
  }
  const table = shallow(<Table {...state}/>)
  const instance = table.instance()

  it('should render Table Component', () => {
  	const props = {
  	  keyNames: ["securitygroup"],
  	  header: ["project_id", "id", "name", "standard_attr_id"],
  	  tables: {}
  	}
  	const bridge = shallow(<Bridge {...props} />)

    expect(bridge.find(Table)).toHaveLength(1)
  })
  it('should input some text', () => {
    const textChange = sinon.spy(instance, 'textChange')

    instance.forceUpdate()
    table.setState(state)
    
    table.state().head.map((head, i) => {
      table.find(TextField).find({name: head}).simulate('change', {
        target: {value: 'aaa'}
      })
      expect(textChange.called).toBe(true)
    })
    textChange.restore()
  })
  it('should click Add Button', () => {
    const add = [{
      table: ["project_id", "id", "name", "standard_attr_id"],
      project_id: '1', id: '1', name: 'one', standard_attr_id: 'one1'
    }]
  	const handleAdd = sinon.spy(instance, 'handleAdd')

  	instance.forceUpdate()
  	table.find(RaisedButton).find({label: 'Add'}).simulate('click')
  	expect(table.state().modal_edit).toBe(true)

    table.setState(state)
    table.find(Dialog)
      .props()
      .actions
      .find(el => el.props.label == 'Add')
      .props
      .onClick('click', {
        target: {value: add}
      })
    expect(handleAdd.calledOnce).toBe(true)
    handleAdd.restore()
  })
  it('should remove row when click', () => {
  	const state = {
  	  header: ["project_id", "id", "name", "standard_attr_id"],
  	  table: [{
  	  	project_id: "qeowqio", id: '0320ewq', name: 'aoi231', standard_attr_id: 'qiewoq' 
  	  }]
  	}
  	const table = shallow(<Table {...state} />)
  	const instance = table.instance()
  	const rowDelete = sinon.spy(instance, 'delete')

  	instance.forceUpdate()
  	table.setState(state)
  	table.find('img').props().onClick('click', {
  	  target: {value: 1}
  	})
  	expect(rowDelete.calledOnce).toBe(true)
  	rowDelete.restore()
  	const expectedTable = []
  	expect(table.state().table).toEqual(expectedTable)
  })
  it('should close dialog when cancel is clicked', () => {
    const handleClose = sinon.spy(instance, 'handleClose')

    instance.forceUpdate()
    table.setState(state)

    table.find(RaisedButton).find({label: 'Add'}).simulate('click')
    expect(table.state().modal_edit).toBe(true)

    table.find(Dialog)
      .props()
      .actions
      .find(el => el.props.label == 'Cancel')
      .props
      .onClick('click')
    expect(table.state().modal_edit).toBe(false)
    expect(handleClose.calledOnce).toBe(true)
  })
})

describe('saveUpdates', () => {
  it('should save file', () => {
    const state = {
      header: ["project_id", "id", "name", "standard_attr_id"],
      table: [{
        project_id: "qeowqio", id: '0320ewq', name: 'aoi231', standard_attr_id: 'qiewoq' 
      }],
      name: ["securitygroup"]
    }
    const table = shallow(<Table {...state} />)
    const file = new Blob(['qqq'], {type: 'text/plain;charset=utf-8'})
    
    table.setState(state)
    var FileSaver = require('file-saver')
    jest.mock('file-saver', () => ({saveAs: jest.fn()}))

    // table.find(RaisedButton).find({label: 'save'}).simulate('click', {
    //   target: {files: [FileSaver.saveAs(file)]}
    // })
  })
})