import React from 'react'
import ReactDOM from 'react-dom'
import App from '../App'
import Group from '../ironman/Group'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import { shallow, mount } from 'enzyme'
import expect from 'expect'

Enzyme.configure({adapter: new Adapter()})

describe('<Group/> Component', () => {
  it('should render <Group/> Component', () => {
  	const group = shallow(<Group />)
  	expect(group).toHaveLength(1)
  })
})