import React from 'react'
import ReactDOM from 'react-dom'
import App from '../App'
import Tool from '../ironman/Tool'
import Enzyme from 'enzyme'
import expect from 'expect'
import Adapter from 'enzyme-adapter-react-16'
import { shallow } from 'enzyme'
import AppBar from 'material-ui/AppBar'

Enzyme.configure({adapter: new Adapter()})

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<App />, div)
})
it('should render <AppBar/>', () => {
  const app = shallow(<App />)

  expect(app.find(AppBar).props().title).toBe('IRONMAN')
})
describe('App Component', () => {
  it('should render Tool Component', () => {
  	const app = shallow(<App />)
  	
  	expect(app.find(Tool)).toHaveLength(1)
  })
})
