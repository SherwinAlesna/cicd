import React from 'react'
import ReactDOM from 'react-dom'
import Tool from '../ironman/Tool'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import { shallow } from 'enzyme'
import expect from 'expect'
import Snackbar from 'material-ui/Snackbar'
import Drawer from 'material-ui/Drawer'
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'
import RaisedButton from 'material-ui/RaisedButton'

Enzyme.configure({adapter: new Adapter()})

describe('Tool popups <Snackbar/> <Drawer/> Component', () => {
  it('should render Snackbar', () => {
  	const tool = shallow(<Tool open={false} />)
  	const instance = tool.instance()
    const handleFileConvert = sinon.spy(instance, 'handleFileConvert')
    const error = 'Please select a file first/Please press the Button Convert Another file.'

    expect(tool.find(Snackbar)).toHaveLength(1)
    instance.forceUpdate()
    tool.find({label: 'Convert File'}).simulate('click')
    tool.find({label: 'Convert File'}).simulate('click')
    expect(handleFileConvert.calledOnce).toBe(true)

    expect(tool.state().message).toBe(error)
    handleFileConvert.restore()
  })
  it('should render Drawer and click "back" button', () => {
    const state = { drawer: true, drawer2: true }
  	const tool = shallow(<Tool />)
    const instance = tool.instance()
    const handleDrawerClose = sinon.spy(instance, 'handleDrawerClose')
    const handleDrawer2Close = sinon.spy(instance, 'handleDrawer2Close')

    expect(tool.find(Drawer)).toHaveLength(2)
    instance.forceUpdate()
    tool.setState(state)

    expect(tool.state().drawer).toEqual(true)
    tool.find(Drawer)
      .at(1)
      .find({label: 'Back'})
      .simulate('click')
    expect(handleDrawerClose.calledOnce).toBe(true)
    expect(tool.state().drawer).toEqual(false)
    handleDrawerClose.restore()

    expect(tool.state().drawer2).toEqual(true)
    tool.find(Drawer)
      .at(0)
      .find(RaisedButton)
      .simulate('click')
    expect(handleDrawer2Close.calledOnce).toBe(true)
    expect(tool.state().drawer2).toEqual(false)
    handleDrawer2Close.restore()
  })
  it('should render DropDown', () => {
    const state = {
      proj: [0,1,2,3]
    }
    const tool = shallow(<Tool />)
    const instance = tool.instance()
    const handleDropDown = sinon.spy(instance, 'handleDropDown')

    instance.forceUpdate()
    console.log(tool.state())
    // if(tool.state().proj.length === tool.state().dropdown){
    //   tool.find(DropDownMenu).simulate('change', {
    //     target: {value: state}
    //   })
    // }
  })
})