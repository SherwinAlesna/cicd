import React from 'react'
import ReactDOM from 'react-dom'
import App from '../App'
import Tool from '../ironman/Tool'
import Bridge from '../ironman/Bridge'
import Table from '../ironman/Table'
import Project from '../ironman/Project'
import Group from '../ironman/Group'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import { shallow } from 'enzyme'
import expect from 'expect'

Enzyme.configure({adapter: new Adapter()})

describe('Tool child Components', () => {
  it('should render <Tool/> Component', () => {
  	const app = shallow(<App />)

  	expect(app.find(Tool)).toHaveLength(1)
  })
  it('should render <Bridge/> Component', () => {
  	const tool = shallow(<Tool />)

  	expect(tool.find(Bridge)).toHaveLength(1)
  })
  it('should render <Table/> Component', () => {
  	const props = {
  	  keyNames: ["securitygroup"],
  	  header: ["project_id", "id", "name", "standard_attr_id"],
  	  tables: {}
  	}
  	const bridge = shallow(<Bridge {...props} />)

    expect(bridge.find(Table)).toHaveLength(1)
  })
  // it('should render <Project/> Component', () => {
  //   const props = {
  //     name: 'q',
  //     project: {
  //       project_id: 'a',
  //       id: 'a',
  //       security_group_id: 'a',
  //       remote_group_id: 'a',
  //       direction: 'a',
  //       ethertype: 'a'
  //     }
  //   }
  //   const project = shallow(<Project {...props} />)
  //   console.log(project.props())
  // })
  it('should render <Group/> Component', () => {
    const group = shallow(<Group />)

    expect(group).toHaveLength((1))
  })
})