import React from 'react'
import ReactDOM from 'react-dom'
import Tool from '../ironman/Tool'
import App from '../App'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import sinon from 'sinon'
import { shallow, mount } from 'enzyme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import expect from 'expect'
import RaisedButton from 'material-ui/RaisedButton'
import './setup'

Enzyme.configure({adapter: new Adapter()})

describe('Tool Component', () => {
  const tool = shallow(<Tool />)
  const instance = tool.instance()

  it('should render all buttons', () => {
    let label = ['Convert File', 'Convert Another File', 'Display in Table', 'Save as JSON', 'Save As SQL', 'Back', 'Display by Project']

    expect(tool.find(RaisedButton)).toHaveLength(8)
    // for(let i = 0; i < label.length; i++){
    //   expect(tool.find(RaisedButton).find({label: label[i]})).toHaveLength(1)
    // }
    expect(tool.find(RaisedButton).find({label: 'Convert File'})).toHaveLength(1)
    expect(tool.find(RaisedButton).find({label: 'Convert Another File'})).toHaveLength(1)
    expect(tool.find(RaisedButton).find({label: 'Display in Table'})).toHaveLength(1)
    expect(tool.find(RaisedButton).find({label: 'Save as JSON'})).toHaveLength(1)
    expect(tool.find(RaisedButton).find({label: 'Save As SQL'})).toHaveLength(1)
    expect(tool.find(RaisedButton).find({label: 'Display by Project'})).toHaveLength(1)
    expect(tool.find(RaisedButton).find({label: 'Back'})).toHaveLength(2)
    expect(tool.find('input')).toHaveLength(1)
  })
  it('should upload a dump file', () => {
    const state = {
      filename: 'json.json'
    }
    const handleFileInput = sinon.spy(instance, 'handleFileInput')
    const file = new Blob(['filename'], {type: 'text/plain'})

    instance.forceUpdate()
    tool.setState(state)
    tool.find('input').simulate('change', {
      target: {files: [file]}
    })
    expect(handleFileInput.calledOnce).toBe(true)
    handleFileInput.restore()
  })
  it('should display your dump file in textarea', () => {
  	const yourFileName = 'some text'

  	tool.find('textarea').simulate('change', {
  		target: {value: yourFileName}
  	})
  	expect(tool.find('textarea').props().value).toEqual(yourFileName)
  })
  it('should click convert your file', () => {
    const yourFileName = 'dump.sql'
    const handleFileConvert = sinon.spy(instance, 'handleFileConvert')

    instance.forceUpdate()
    expect(tool.state().ctr).toEqual(0)
    tool.find({label: 'Convert File'}).simulate('click')
    tool.find({label: 'Convert File'}).simulate('click')
    expect(handleFileConvert.calledOnce).toBe(true)
    expect(tool.state().ctr).toEqual(1)
    handleFileConvert.restore()
  })
  it('should reset when "Convert Another File" is click', () => {
    const clearState = sinon.spy(instance, 'clearState')

    instance.forceUpdate()
    tool.find('textarea').simulate('change', {
      target: {files: [new Blob(['json.json'], {type:'text/plain'})]}
    })

    expect(tool.state().filename).toBe('json.json')
    tool.find({label: 'Convert Another File'}).simulate('click')
    expect(clearState.calledOnce).toBe(true)
    expect(tool.state().filename).toBe('')

    clearState.restore()
  })
  it('shoudl click Save As SQL', () => {
    const state = {
      tables: {securitygrouprules: {project_id: "'71349bb5997f4a5e8c6493c2077ef06b'"}},
      textarea: ''
    }
    const handleJsonParse = sinon.spy(instance, 'handleJsonParse')

    instance.forceUpdate()
    tool.setState(state)
    tool.find({label: 'Save As SQL'}).simulate('click')
    expect(handleJsonParse.calledOnce).toBe(true)
    handleJsonParse.restore()
  })
  it('should toggle when Display in Table is click', () => {
    const state = {
      drawer: false
    }
    const handleToggle = sinon.spy(instance, 'handleToggle')

    instance.forceUpdate()
    tool.setState(state)

    expect(tool.state().drawer).toEqual(false)
    tool.find({label: 'Display in Table'}).simulate('click')
    expect(tool.state().drawer).toBe(true)
    expect(handleToggle.calledOnce).toBe(true)

    handleToggle.restore()
  })
  it('should click Save as JSON', () => {
    const state = {
      filename: 'json.json',
      file: 'riewoi'
    }
    const contents = 'file contents'
    const tool = shallow(<Tool />)
    const instance = tool.instance()
    const saveAsJSON = sinon.spy(instance, 'saveAsJSON')
    const handleFileInput = sinon.spy(instance, 'handleFileInput')
    const handleTextAreaChange = sinon.spy(instance, 'handleTextAreaChange')
    const file = new Blob(['dump.sql'], {type: 'text/plain'})

    instance.forceUpdate()
    tool.setState(state)
    tool.find('input').simulate('change', {
      target: {files: [file]}
    })
    expect(handleFileInput.calledOnce).toBe(true)

    tool.find('textarea').simulate('change', {
      target: {value: contents}
    })
    expect(handleTextAreaChange.calledOnce).toBe(true)
    expect(tool.state().textarea).toBe(contents)

    var FileSaver = require('file-saver')
    jest.mock('file-saver', () => ({saveAs: jest.fn()}))

    tool.find({label: 'Save as JSON'}).simulate('click', {
      target: {files: [FileSaver.saveAs(file)]}
    })
    expect(saveAsJSON.calledOnce).toBe(true)

    saveAsJSON.restore()
    handleFileInput.restore()
    handleTextAreaChange.restore()
  })
})  