import React, { Component } from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import Tool from './ironman/Tool'

class App extends Component {
  render() {
    console.error = () => {
      var error = console.error

      return function(exception){
        if ((exception + '').indexOf('Warning: A component is `contentEditable`') !== 0){
          error.apply(console, arguments)
        }
      }
    }

    let ironman = <div>
      <AppBar 
        style={{backgroundColor: "#DB6B71"}}
        titleStyle={{color:"#fff", fontWeight:'1px' }} 
        title="IRONMAN"
        showMenuIconButton={false}
      />
      <Tool supressContentEditableWarning={true} />
    </div>
    return (
      <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
        {ironman}
      </MuiThemeProvider>
    );
  }
}

export default App;
