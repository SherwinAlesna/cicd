import React, { Component} from 'react'
import './Table.css'

class Project extends Component{
  constructor(props){
  	super(props)
  	this.state = {
  	  proj: [],
      project: [],
      table: []
  	}
  }
  componentWillUpdate(nextProps){
    if(this.props.project !== nextProps.project){
      this.setState({
        project: {
          name: nextProps.name,
          proj: nextProps.project
        },
        table: nextProps.project.length
      })
    }
  }
  remove(ndx){
    let table = this.props.project
    table.splice(ndx,1)
    this.setState({table})
  }
  open(){
    const { project, table } = this.state

    project.proj.map((p, i) => {
      console.log('p.id', p.id)
      console.log('p["id"]', p['id'])
    })
    console.log(table)
  }
  render(){
    return(
      <div>
      <button onClick={this.open.bind(this)}>PROJECT COnSOLE</button>
        <section style={{paddingTop:'5px'}}>
        <h3>Security Groups Rules</h3>
          <div className="table_div">
      	  <table 
            cellPadding="0" 
            cellSpacing="0" 
            border="0" 
          >
      	    <thead className = "tbl-header">
      	      <tr>
      	        <th>project_id</th>
      	        <th>ID</th>
      	        <th>security_group_id</th>
      	        <th>remote_group_id</th>
      	        <th>direction</th>
      	        <th>ethertype</th>
      	        <th>protocol</th>
                <th>port_range_min</th>
                <th>port_range_max</th>
                <th>remote_ip_prefix</th>
                <th>standard_attr_id</th>
                <th></th>
      	  	  </tr>
      	    </thead>
      	    <tbody className ="tbl-content" >




              {
              	this.props.project.map((proj,ndx)=>{
              		return(
                      <tr key={ndx}>
                        <th>{proj['project_id']}</th>
                        <th>{proj['id']}</th>
                        <th>{proj['security_group_id']}</th>
                        <th>{proj['remote_group_id']}</th>
                        <th>{proj['direction']}</th>
                        <th>{proj['ethertype']}</th>
                        <th>{proj['protocol']}</th>
                        <th>{proj['port_range_min']}</th>
                        <th>{proj['port_range_max']}</th>
                        <th>{proj['remote_ip_prefix']}</th>
                        <th>{proj['standard_attr_id']}</th>
                        <td>
                          <img 
                            alt='img'
                            src="https://png.icons8.com/trash/androidL/20/FFFFFF"
                            style={{cursor:'pointer'}}
                            onClick={this.remove.bind(this, ndx)}
                          />
                        </td>
                      </tr>  
              		)
              	})
              }
            </tbody>
      	  </table>
          </div>
        </section>
      </div>
    )
  }
}

export default Project