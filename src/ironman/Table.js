import React, {Component} from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import HtmlTableToJson from 'html-table-to-json'
import './Table.css'

class Table extends Component{
  constructor(props){
    super(props);
    this.state={
      table:this.props.table,
      modal_add: false,
      modal_edit:false,
      texta: {}
    };
    this.parseTable = this.parseTable.bind(this)
    this.handleAdd = this.handleAdd.bind(this)
    this.buttonAdd = this.buttonAdd.bind(this)
    this.textChange = this.textChange.bind(this)
  }

  parseTable(){
    var table = document.getElementById(this.props.name);
    var obj = new HtmlTableToJson('<table>' + table.innerHTML + '</table>');

    console.log(table)
    console.log('___________-')
    console.log(obj)
    this.saveUpdates(obj.results);
  }

  saveUpdates(obj){
    var col = this.props.header;
    var newObj = obj;
    var sql = "";

    sql="\n INSERT INTO `" + this.props.name + "` VALUES " + 
    newObj[0].map((obj)=>{
      return(
        "(" + col.map((field)=>{
          return obj[field]
        }) + ")"
      )   
    }) + ";";

    var FileSaver = require('file-saver');
    var blob = new Blob([sql],{type:"text/plain;charset=utf-8"});
    var filename = "edited_" + this.props.name + ".sql";

    FileSaver.saveAs(blob,filename);

  }
  delete(ndx){
    let table = this.state.table;
    table.splice(ndx,1);
    this.setState({table:table});
  }
  
  handleClose = () => {
    this.setState({modal_edit: false});
  }

  buttonAdd(){
    this.setState({modal_edit:true})
  }
  handleAdd(){
    const { texta, table } = this.state
    let temp = {};

    temp = {...texta}
    table.push(temp);

    this.setState({
      table,
      modal_edit:false
    });
  }
  textChange(e){
    let name = e.target.name
    let value = e.target.value
    
    this.setState({
      texta: {
        ...this.state.texta,
        [name]: value
      }
    })
  }

  render(){

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
        labelStyle={{color:'#DB6B71'}}
      />,
      <FlatButton
        label="Add"
        primary={true}
        onClick={this.handleAdd}
        labelStyle={{color:'#DB6B71'}}
      />,
    ];
    return(
      <section style={{paddingTop:'20px'}}>
        <h3 style={{textTransform:'uppercase'}}>{this.props.name}</h3>
        <div className="table_div">
        <table id ={this.props.name} cellPadding="0" cellSpacing="0" border="0" >
          <thead className = "tbl-header">
            <tr>
              {this.props.header.map((head, index)=>{
                return(
                  <th key={index}>{head}</th>
                );
              })}
                <th></th>
            </tr>
          </thead>
          <tbody className ="tbl-content" >
            {
              this.state.table.map((obj,ndx)=>{
                return(
                  <tr key={ndx} >
                    {
                      this.props.header.map((head, i)=>{
                        return(
                          <td key={i} contentEditable='true' >{obj[head]}</td>
                        );
                      })
                    }
                    <td>
                      <img 
                        alt='something something'
                        src="https://png.icons8.com/trash/androidL/20/FFFFFF"
                        style={{cursor:'pointer'}}
                        onClick={this.delete.bind(this, ndx)}
                      />
                    </td>
                  </tr>
                );
              })
            }
          </tbody>
        </table>
        </div>
        <RaisedButton
          style={{marginTop:'10px',marginRight:'10px'}}
          label="save"
          onClick={this.parseTable}
          backgroundColor = "#DB6B71"
          labelColor="#fff"
          overlayStyle={{border:'solid 2px #fff '}}
        />
        <RaisedButton
          style={{marginTop:'10px',marginRight:'10px'}}
          label="Add"
          onClick={this.buttonAdd}
          backgroundColor = "#DB6B71"
          labelColor="#fff"
          overlayStyle={{border:'solid 2px #fff '}}
        />
        <Dialog
          title={this.props.name}
          actions={actions}
          modal={false}
          open={this.state.modal_edit}
          onRequestClose={this.handleClose}
          titleStyle={{backgroundColor:'#DB6B71',color:'#fff'}}
          autoScrollBodyContent={true}
        >
          <div style={{marginLeft:'20px',marginTop:'20px'}}>
            {
              this.props.header.map((head, i)=>{
                return(
                    <div key={i}>
                    <h4 style={{padding:'0px',margin:'0px'}}>{head}</h4>
                    <TextField
                      id={head}
                      value={this.state.texta.value}
                      name={head}
                      onChange={this.textChange}
                      type="text"
                      inputStyle={{paddingBottom:'1px'}}
                    />
                  </div>
                );
              })
            }
          </div>
        </Dialog>
      </section>
    );
  }
}

export default Table;