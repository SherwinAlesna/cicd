import React, {Component} from 'react'
import Table from './Table'

class Bridge extends Component{
  render(){
  	return(
      <div>
          {this.props.keyNames.map((name, ndx) => {
            return(
                <Table
                  key={ndx}
                  table={this.props.tables[name]}
                  name={name}
                  header={this.props.header[ndx]}
                  arr_tables={this.props.keyNames}
                />
            )
          })}
      </div>
  	)
  }
}

export default Bridge