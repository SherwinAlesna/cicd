import React, { Component } from 'react'
import './Table.css'

class Group extends Component{
  constructor(props){
  	super(props)
  	this.state = {
  	  group: []
  	}
  }
  componentWillUpdate(nextProps){
    if(this.props.group !== nextProps.group){
      this.setState({
        group: nextProps.group
      })
    }
  }
 render(){
   return(
     <div>
       <section style={{paddingTop:'5px'}}>
         <h3>Security Groups</h3>
         <div className="table_div">
           <table cellPadding="0"  cellSpacing="0"  border="0">
             <thead className = "tbl-header">
               <tr>
                 <th>project_id</th>
                 <th>ID</th>
                 <th>name</th>
                 <th>standard_attr_id</th>
                 <th></th>
               </tr>
             </thead>
             <tbody className ="tbl-content" >
               {
                 this.state.group.map((proj,ndx)=>{
                   return(
                     <tr key={ndx}>
                       <th>{proj.project_id}</th>
                       <th>{proj.id}</th>
                       <th>{proj.name}</th>
                       <th>{proj.standard_attr_id}</th>
                       <td>
                       </td>
                     </tr>  
                   )
                 })
               }
             </tbody>
           </table>
         </div>
       </section>
     </div>
   )
 }
}

export default Group