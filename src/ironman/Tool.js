import React, {Component} from 'react'
import {Card, CardText} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import Snackbar from 'material-ui/Snackbar'
import Drawer from 'material-ui/Drawer'
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'
import Bridge from './Bridge'
import Group from './Group'
import Project from './Project'

class Tool extends Component{
  constructor(props){
  	super(props)
  	this.state = {
  		filename:"",
  		file: "",
	    data: [],
	    header: [],
	    tables:{},
      message: 'Please select a file first/Please press the Button Convert Another file.',
	    keyNames: [],
	    status: 0,
	    drawer: false,
	    str_table: "",
	    textarea: "",
      open: false,
      ctr: 0,
      types: [],
      proj: [],
      disabled: true,
      disabled1: true,
      drawer2: false,
      dropdown: 0,
      current: [],
      current_name: "",
      menus: [],
      groups: [],
      current_group: []
  	}
  	this.handleFileInput = this.handleFileInput.bind(this)
  	this.handleFileConvert = this.handleFileConvert.bind(this)
  	this.parse = this.parse.bind(this)
  	this.handleClose = this.handleClose.bind(this)
  	this.handleDialogOpen = this.handleDialogOpen.bind(this)
  	this.saveAsJSON = this.saveAsJSON.bind(this)
  	this.handleJsonParse = this.handleJsonParse.bind(this)
    this.clearState = this.clearState.bind(this)
  	this.handleTextAreaChange = this.handleTextAreaChange.bind(this)
    this.handleRequestClose = this.handleRequestClose.bind(this)
    this.toggleDrawer = this.toggleDrawer.bind(this)
    this.handleDrawer2 = this.handleDrawer2.bind(this)
    this.handleDropDown = this.handleDropDown.bind(this)
  }

  handleToggle = () => this.setState({drawer: !this.state.drawer})
  handleDrawerClose = () => this.setState({drawer: false})
  toggleDrawer = () => this.setState({drawer: !this.state.drawer})
  toggleDrawer2 = () => this.setState({drawer: !this.state.drawer2})
  handleDrawer2Close = () => this.setState({drawer2: false})

  handleClose(){
    this.setState({dialog: false,tables:{}})
  }

  handleDialogOpen(){
  	 this.setState({dialog: true})
  }
  handleTouchTap(){
    this.setState({open: true})
  }

  handleRequestClose(){
    this.setState({open: false})
  }

  clearState(){
    const initialState = {
      filename: "",
      file: "",
      data: [],
      header: [],
      tables:{},
      keyNames: [],
      status: 0,
      textarea: "",
      ctr:0,
      types: [],
      proj: [],
      disabled: true,
      disabled1: true,
      dropdown: 0,
      current: [],
      current_name: "",
      menus: [],
      groups: [],
      current_group: []
    }
    this.setState(initialState)

    // this.refs.files.value = this.state.filename // commented for test only
  }

  parse(statement){
    var tables = this.state.tables
  	var newStatement = statement.split('(')
  	var tableName = statement.split(' ')[2]

  	tables[tableName] = tables[tableName] || []
    for(var i = 1; i < newStatement.length; i++){
	    let txt = newStatement[i].split(')')[0]
	    let val = txt.match(/('.*?'|[^",\s]+)(?=\s*,|\s*$)/g)
	    let temp = []
        
      val.map((x)=>{
	      temp.push(x)
	      return 0
	    })
	    tables[tableName].push(temp)
    }
    this.setState({ tables })
  }

  handleFileConvert(){
  	var ctr = 0
      if(this.state.filename !== 0 && this.state.ctr === 0){
        var tables = this.state.tables
  	    var lines = this.retrieveInsertStatements()
        let create = this.retrieveCreateTableStatements()
        let obj = this.extractTableFields(create)
        let col = obj.fields
        let types = obj.types
        let length = lines.length

		    for(let x =0; x < length; x++){
			    this.parse(lines[x])
		    }

		    var keyNames = Object.keys(this.state.tables)
		    const shape = require('shape-array')
		    var header = this.state.header

        for(var x in keyNames){
          let temp = []
          let name = keyNames[x]
          let table = tables[name]
          let fields = []
          let datas = []
          let dbtable = []

          temp = table
          fields = col[x]
          var personify = shape.scheme(fields)
          datas = temp

          for(var y in datas){
            let data = datas[y]
            let row = personify(data)
            dbtable.push(row)
          }

          tables[name]=dbtable
          this.setState({ tables })
          header.push(fields)

          temp = []
          name = []
          table=[]
          fields = []
          datas = []
          dbtable = []
          fields = []
        }

      let proj = []
      proj = this.joinObject(keyNames, this.state.tables)

      ctr++
      if(this.state.filename.includes('securitygroups')){
        this.setState({
          ctr,
          keyNames,
          header,
          textarea: JSON.stringify(this.state.tables,null,2),
          types,
          proj,
          disabled: false,
          disabled1: false
        })
      }else{
        this.setState({
          ctr,
          keyNames,
          header,
          types,
          proj,
          textarea: JSON.stringify(this.state.tables, null, 2),
          disabled: false
        })
      }
  	}else{
  		this.handleTouchTap()
  	}
  }
  extractTableFields(lines){
    let length = lines.length
    let fields = []
    let temp1 = []
    let temp2 = []
    let types = []
    let temp = {
      fields: [],
      types: []
    }

    for(let x = 0; x < length; x++){
      fields = []
      types = []

      if(lines[x].includes('CREATE TABLE')){
        for(let y = x + 1;y < length && !lines[y].includes('CREATE TABLE'); y++){
          lines[y] = lines[y].replace(/`/g,"")
          let words = lines[y].split(' ')
          fields.push(words[2])
          types.push(words[3])
          x = y
        }
        temp1.push(fields)
        temp2.push(types)
      }
    }
    temp.fields = temp1
    temp.types = temp2
    return temp
  }
  retrieveCreateTableStatements(){
    let lines = this.state.file.split("\n")
    let length = lines.length
    let createStatements = []

    for(let x = 0; x < length; x++){
      if(lines[x].includes('CREATE TABLE')){
        createStatements.push(lines[x])
        for(let z = x + 1; z < length && !lines[z].includes(";"); z++){
          if(this.checkDataType(lines[z])){
            createStatements.push(lines[z])
          }
          x = z
        }
      }
    }
    return createStatements
  }
  joinObject(names, tables){
    let group = []
    let groups = []
    let proj = []
    let project = []
    let rules = []
    
    for(let x in names){
      if(names[x] === 'securitygroups'){
        groups = tables[names[x]]
      }else if(names[x] === 'securitygrouprules'){
        rules = tables[names[x]]
      }else{
        proj = tables[names[x]]
      }
    }

    for(let x = 0; x < proj.length; x++){
      let temp = []
      let temp2 = []

      for(let y = 0; y < rules.length; y++){
        if(proj[x].id === rules[y].project_id){
          temp.push(rules[y])
        }
      }
      for(let z = 0; z < groups.length; z++){
        if(proj[x].id === groups[z].project_id){
          temp2.push(groups[z])
        }
      }
      group.push(temp2)
      project.push(temp)
    }
    this.setState({
      menus: proj,
      groups: group
    })
    return project;
  }
  checkDataType(statement){
    if(statement.includes("varchar") ||
      statement.includes("char") ||
      statement.includes("enum") ||
      statement.includes("int") ||
      statement.includes("bigint") ||
      statement.includes("timestamp") ||
      statement.includes("datetime") ||
      statement.includes("text") ||
      statement.includes("date") ||
      statement.includes("float")
    ){
      return true
    }
    return false
  }
  retrieveInsertStatements(){
    var lines = this.state.file.split("\n")
    var temp = []

    for(var y=0,z=y; y < lines.length; y++){
    	var line = lines[y]
    	if(this.is_insert(line)===true){
    		if(line.includes("")===false){

    			for(z = y+1; z < lines.length && !lines[z].includes(""); z++){
              line = line + lines[z]
            }
          line = line + lines[z]
          y=z
    		}
    		line = line.split('`').join('')
        temp.push(line)
    	}
    }
    lines = []
    return temp
  }

  handleFileInput(evt){
  	var fr = new FileReader()
  	var file = evt.target.files[0]
    let qwe = evt.target.files[0].name

    fr.addEventListener('load', (e) => {
      var contents = e.target.result

      this.setState({
        filename: qwe,
        file: contents.replace(/[\r]+/g, '')
      })
    })
  	fr.readAsText(file)
  }

  is_insert(line){
    if( line.startsWith("INSERT INTO") || line.includes("INSERT INTO")){
    	return true
    }
    return false
  }

  is_validLine(line){
    if(!this.is_insert(line) && line!== " " && 
      line!=="\n" && !line.startsWith("--") && 
      !line.startsWith("/*") && 
      !line.startsWith("SELECT") &&
      (!line.startsWith("(")) &&
      !line.startsWith("COMMIT") &&
      !line.startsWith("SET") &&
      !line.startsWith("START")
    ){
      return true
    }
    return false
  }

  handleTextAreaChange(event){
    this.setState({
      textarea: event.target.value
    })
  }
  
  handleJsonParse(){
      if(this.state.filename !== 0){
      var newObj = this.state.tables
      var sql =""

      if(this.state.textarea!==""){
        newObj = JSON.parse( this.state.textarea )
        sql = "" + 
            this.state.keyNames.map((name,ndx)=>{
              return(
                 "INSERT INTO `" + name + "` VALUES " + 
                newObj[name].map((obj)=>{
                  return(
                     "(" + 
                        this.state.header[ndx].map((field)=>{
                          return obj[field]
                        }) + ")"
	              )	
                })
              ) + ";" }).join('\n')

        var lines = this.state.file.split("\n")
        var file=""

        file = lines.map((line)=>{
          if(this.is_validLine(line)){
            return line
          }
          return console.log("")
        }).join('\n')
	    
        file+=sql

        var FileSaver = require('file-saver')
        var blob = new Blob([file],{type:"text/plain;charset=utf-8"})

        FileSaver.saveAs(blob,'converted_' + this.state.filename) 
        lines = []
      }
    }else{
      this.handleTouchTap()
    }
  }

  saveAsJSON(){
    if(this.state.filename !== 0){
      var FileSaver = require('file-saver')
      var blob = new Blob([this.state.textarea],{type:"text/plain;charset=utf-8"})
      var filename = "converted-json.json"
      
      FileSaver.saveAs(blob, filename)
    }else{
       this.handleTouchTap()
    }
  }

  handleDrawer2(){
    this.setState({
      current: this.state.proj[0],
      current_name: this.state.menus[0]['name'],
      current_group: this.state.groups[0],
      drawer2: true
    })
  }
  handleDropDown(event, index, value){
    let temp = []
    let name = ''
    let group = []

    console.log(this.state.proj[value])
    if(this.state.proj[value].length !== 0 && this.state.groups[value].length !== 0){
      temp = this.state.proj[value]
      group = this.state.groups[value]
    }

    name = this.state.menus[value]['name']
    this.setState({
      dropdown: value,
      current: temp,
      current_name: name,
      current_group: group
    })
  }
  render(){
  	return(
  	  <Card >
  	  	<CardText>
  	  		 <input
            placeholder="import"
            type="file"
            id="files"
            ref="files"
            name="files[]"
            onChange={this.handleFileInput}
          />
          <RaisedButton
            style={{marginTop:'10px',marginRight:'10px'}}
            label="Convert File"
            onClick={this.handleFileConvert}
            backgroundColor = "#DB6B71"
            labelColor="#fff"
          />
          <RaisedButton
            style={{marginTop:'10px',marginLeft:'10px'}}
            label="Convert Another File"
            onClick={this.clearState}
            backgroundColor = "#DB6B71"
            labelColor="#fff"
            disabled={this.state.disabled}
          />
          <RaisedButton
            style={{marginTop:'10px',marginLeft:'10px'}}
            label="Display in Table"
            onClick={this.handleToggle}
            backgroundColor = "#DB6B71"
            labelColor="#fff"
            disabled={this.state.disabled}
          />
          <RaisedButton
            style={{marginTop:'10px', marginLeft:'10px'}}
            label='Display by Project'
            onClick={this.handleDrawer2}
            backgroundColor='#DB6B71'
            labelColor='#fff'
            disabled={this.state.disabled1}
          />
          <br/><br/>
          <textarea 
            rows="26" 
            cols="155"
            value={this.state.textarea}
            onChange ={this.handleTextAreaChange}
            style={{resize:"none"}}
          />
          <RaisedButton
            style={{marginTop:'10px',marginRight:'10px'}}
            label="Save as JSON"
            onClick={this.saveAsJSON}
            backgroundColor = "#DB6B71"
            labelColor="#fff"
            disabled={this.state.disabled}
          />
          <RaisedButton
            style={{marginTop:'10px'}}
            label="Save As SQL"
            onClick={this.handleJsonParse}
            backgroundColor = "#DB6B71"
            labelColor="#fff"
            disabled={this.state.disabled}
          />
          <Snackbar
            open={this.state.open}
            message={this.state.message}
            autoHideDuration={2000}
            onRequestClose={this.handleRequestClose}
            style={{textAlign:"center"}}
          />
          <Drawer
            docked={false}
            width={1350}
            open={this.state.drawer2}
            onRequestChange={this.toggleDrawer2}
            containerStyle={{backgroundColor:'#DB6B71'}}
          >
            <RaisedButton
              style={{marginTop:'10px', marginLeft:'20px'}}
              label='Back'
              backgroundColor='#DB6B71'
              labelColor='#fff'
              onClick={this.handleDrawer2Close}
              overlayStyle={{border:'solid 2px #fff'}}
            />
            <div style={{paddingTop:'10px', paddingLeft:'10px'}}>
              <DropDownMenu
                value={this.state.dropdown}
                onChange={this.handleDropDown}
                labelStyle={{color:'#fff', fontWeight:'10px'}}
                autoWidth={false}
                style={{width:'250px', marginLeft:'23px'}}
              >
                {this.state.menus.map((project, i) => {
                  return(
                    <MenuItem
                      key={i}
                      value={i}
                      primaryText={project['name']}
                    />
                  )
                })}
              </DropDownMenu>
              <div style={{paddingLeft:'42px'}}>
                <h2>PROJECT NAME: {this.state.current_name}</h2>
              </div>
              <Group
                group={this.state.current_group}
              />
              <Project
                name={this.state.current_name}
                project={this.state.current}
              />
            </div>
          </Drawer>
          <Drawer
            docked={false}
            width={1400}
            open={this.state.drawer}
            onRequestChange={this.toggleDrawer}
            containerStyle={{backgroundColor:'#DB6B71'}}
          >
            <RaisedButton
              style={{marginTop:'10px',marginLeft:'20px'}}
              label="Back"
              backgroundColor = "#DB6B71"
              labelColor="#fff"
              onClick={this.handleDrawerClose}
              overlayStyle={{border:'solid 2px #fff '}}
            />
              <Bridge
                keyNames={this.state.keyNames}
                tables={this.state.tables}
                header={this.state.header}
              />
          </Drawer>
  	  	</CardText>
  	  </Card>
  	)
  }
}

export default Tool