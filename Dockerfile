FROM node:latest

RUN mkdir -p /src/ironman
WORKDIR /src/ironman

COPY package.json /src/ironman
COPY package-lock.json /src/ironman

RUN npm install

COPY . /src/ironman
 
EXPOSE 3000

ENTRYPOINT ["/bin/bash", "/src/ironman/run.sh"]
CMD ["start"]
