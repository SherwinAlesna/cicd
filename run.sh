#!/usr/bin/env bash
set -eo pipefail

case $1 in
  start)
    npm start | cat
    ;;
  build)
    echo "building ironman"
    npm run-script build --env=prod
    ;;
  test)
    echo "testing ironman"
    npm test
    ;;
  port)
    echo "starting ironman at 'https://localhost:3001'"
    ;;
  *)
    exec "$@"
    ;;
esac
