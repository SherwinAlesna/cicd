IRONMAN
================
A tool that attempts to convert dump files into a more human-readable format(i.e. HTML tables and JSON object).

### PREREQUISITES
 - Node.js

### INSTALLATION
Clone the project. 
Install the packages in the project through the ` npm install --save ` command. 

#### RUNNING THE PROJECT
 ` npm start ` 
 
#### Note : If there are errors related to the packages while running the project, try installing the package error
 
 Example:
 ```
 npm install --save shape-array 
 ```

#### More about Ironman
- Works by locating ` INSERT INTO ` statements from the imported dump file. 
- The parsed dump file can be easily modified through the text area or table. 


#### LIMITATIONS: 
 - When ` INSERTING ` or ` UPDATING ` a data, be sure to add quotation marks if it's a string or an enum. 
 - May not parse other ` INSERT INTO ` statements if one of that statement contains alot of data.
  - Solution: separate the other ` INSERT INTO ` statements from the imported dump file in another file.
  
 - Fields must be present in the ` INSERT INTO ` statements in the dump file.
 
    -  Example:
     
     ```
      ` INSERT INTO  users (user_id, firstname, lastname, username, password) ` VALUES ` (1, 'Tony', 'Stark', 'ironman', yx1cwudi3g), (2, 'Thor', 'Odinson', 'mjollnir', 1x1cwudi3g), (3, 'Bruce', 'Banner', 'Hulk', asx1cwudi3g); `
     ```
     

### TO DO:
#### (1). Run the project.
<p align="center">
  <img src="screenshots/Home.PNG" width="1000"/>
</p>

#### (2). Select dump file.
 - Click on the ` browse ` button and select the sample dump file in the project.

<p align="center">
  <img src="screenshots/select.PNG" width="1000"/>
</p>

#### (3). Converting the dump file.
 - After selecting the dump file, click ` CONVERT FILE ` button.
 - The data in the textarea represents the data per table coming from the selected dump file converted as JSON object.

<p align="center">
  <img src="screenshots/converted.PNG" width="1000"/>
</p>

#### (4). Displaying the converted data as Data Tables.
 - Click on the ` DISPLAY TABLE ` button.

<p align="center">
  <img src="screenshots/displaytable.PNG" width="1000"/>
</p>

#### (5). Inserting data.
 - Click the ` Add ` button.
 - A form should be displayed on the screen containing the fields of the table.
 - Try inputting valid data on the text fields, then click ` ADD `.

<p align="center">
  <img src="screenshots/add.PNG" width="1000"/>
</p>

 - After adding, the newly added data is now added on the table.

<p align="center">
  <img src="screenshots/added.PNG" width="1000"/>
</p>
 
 - Clicking the ` SAVE ` button converts the data table into an sql file containing the newly added data.

<p align="center">
  <img src="screenshots/save.PNG" width="1000"/>
</p>
<p align="center">
  <img src="screenshots/open-modifieddump.PNG" width="1000"/>
</p>
<p align="center">
  <img src="screenshots/newdump.PNG" width="1000"/>
</p>

#### (6). Delete.
 - The trash icon in the table represents the delete action.
 - Try to delete the newly added data and other data in the table.

<p align="center">
  <img src="screenshots/deleted.PNG" width="1000"/>
</p>
 
 - Then click ` SAVE `.
 - Open the newly saved sql file.
 - In the sql file, you can see the ` INSERT INTO ` statements without the deleted data.

<p align="center">
  <img src="screenshots/sqldeleted.PNG" width="1000"/>
</p>
 
#### (7). Edit data.
 - Editing the values of a certain row can simply be done by clicking the column of the specific row/data that you want to edit.
 - In the screenshot below, you can see that the CardAmount of the row containing CardID:84 were change from 100 to 99999.

<p align="center">
  <img src="screenshots/edit1.PNG" width="1000"/>
</p>
<p align="center">
  <img src="screenshots/edit2.PNG" width="1000"/>
</p>

 - After editing the values, click ` SAVE `.
 -  Clicking the save button saves a newly sql file containing an ` INSERT INTO ` statement with newly edited values.

<p align="center">
  <img src="screenshots/edit3.PNG" width="1000"/>
</p>

#### Additional Process:
 - You can Add, Update and Delete data by simply copying, pasting, erasing and editing the object values in the textarea, then click ` Save As Sql ` button. 
 - Only edit values inside quotation marks and be sure to follow the format of an JSON Object.
